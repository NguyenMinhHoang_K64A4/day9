<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Result</title>
    </head>
<body>
<?php
require_once 'answer.php';
if (isset($_COOKIE['answer'])) {
        $chooseanswer = json_decode($_COOKIE['answer'], true);
        $chooseanswer = $_POST + $chooseanswer;
        $sum_choice = checkQuestions($chooseanswer);
        $result = '';
        if ($sum_choice > 7) {
            $result = "Số điểm: " . $sum_choice . ". Sắp sửa làm được trợ giảng lớp PHP.";
        } elseif ($sum_choice > 4 && $sum_choice < 7) {
            $result = "Số điểm: " . $sum_choice . ". Cũng bình thường.";
        } else {
        $result = "Số điểm: " . $sum_choice . ". Bạn quá kém, cần ôn tập thêm.";
    }
    setcookie("page", "", time() - 3600);
    setcookie("answer", "", time() - 3600);
}
function checkQuestions($chooseanswer)
{
    $sum_choice = 0;
    foreach ($chooseanswer as $keyQuestion => $userchoice) {
        if (array_key_exists($keyQuestion, QUESTIONS)) {
            if (ANSWERS[$keyQuestion]['result'] == $userchoice) {
                $sum_choice++;
            }
        }
    }
    return 10 / count(QUESTIONS) * $sum_choice;
}
?>
    <h1><?= $result?></h1>
</form>
</body>
</html>