<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quizz</title>
</head>
<body>
<?php
require_once 'answer.php';
$chooseanswer = array(); 

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    if (isset($_POST['nextPage'])) {
        if (isset($_COOKIE['page'])) {
            setcookie('page', $_COOKIE['page'] + 1);
            $_COOKIE['page'] = $_COOKIE['page'] + 1;
            if (isset($_COOKIE['answer'])) {
                $chooseanswer = json_decode($_COOKIE['answer'], true);
                $chooseanswer = $_POST + $chooseanswer;
                setcookie('answer', json_encode($chooseanswer));
            } else {
                setcookie('answer', json_encode($_POST));
            }
        } else {
            setcookie('page', 1);
            $_COOKIE['page'] = 1;
        }
    }
    if (isset($_POST['previous'])) {
        if (isset($_COOKIE['page'])) {
            setcookie('page', $_COOKIE['page'] - 1);
            $_COOKIE['page'] = $_COOKIE['page'] - 1;
            $chooseanswer = json_decode($_COOKIE['answer'], true);
            $chooseanswer = $_POST + $chooseanswer;
            setcookie('answer', json_encode($chooseanswer));
        } else {
            setcookie('page', 1);
            $_COOKIE['page'] = 1;
        }
    }
}
function NextPage($array, $end)
{
    return true ? $end < count($array) : false;
}
function Choice($chooseanswer, $keyQuestion, $answer)
{
    if (isset($chooseanswer[$keyQuestion]) && $chooseanswer[$keyQuestion] == $answer) {
        return true;
    }
    return false;
}
if (!isset($_COOKIE['page']) || $_COOKIE['page'] <= 0) {
setcookie('page', 1);
    $_COOKIE['page'] = 1;
}
?>
<form method="POST" action="result.php">
        <div class="questions">
                <?php $questionNum = $_COOKIE['page'] * Number - Number;
                foreach (array_slice(QUESTIONS, ($_COOKIE['page'] - 1) * Number, Number, true) as $keyQuestion => $question) :
                    $questionNum = $questionNum  + 1;
                ?>
                <div class="question">
                    <h3><span>Câu hỏi <?= $questionNum ?>:</span> <?= $question ?>
                    </h3>
                    <div class="answer">
                        <?php foreach (ANSWERS[$keyQuestion]['answers'] as $answer) : ?>
                        <input type="radio" id="<?= $answer . $question ?>" name="<?= $keyQuestion ?>"
                            value="<?= $answer ?>"
                            <?php if (Choice($chooseanswer, $keyQuestion, $answer)) echo 'checked'; ?>>
                        <label for="<?= $answer . $question ?>"> <?= $answer ?> </label><br>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endforeach; ?>

            <div class="action">
                <?php
                if (NextPage(QUESTIONS, $questionNum)) {
                    echo '<button type="submit" name="nextPage"  formaction="" formethod="POST">nextPage</button>';
                } else {
                    echo '<button type="submit" name="previous" formaction="" formethod="POST">Back</button>';
                    echo '<button type="submit" name="submit">Submit</button>';
                }
                ?>

            </div>
        </div>
    </form>
</body>
</html>