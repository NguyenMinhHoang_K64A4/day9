<?php
define("Number", 5);
define("QUESTIONS", array(
    '0' => 'Will you marry me ?',
    '1' => 'Are you ten years old',
    '2' => 'Are you seeing anyone ?', 
    '3' => 'Is your last name Anna ?',
    '4' => '1 + 1 = ?', 
    '5' => '1 % 2 = ?',
    '6' => '2 / 1 = ?', 
    '7' => '3 + 1 = ?',
    '8' => '2 x 3 = ?', 
    '9' => '5 * 10 = ?',
    // '10' => '3 * 4 = ?',
));
define("ANSWERS", array(
    '0' => array('answers' => array('yes', 'no'), 'result' => 'yes'),
    '1' => array('answers' => array('yes', 'no'), 'result' => 'no'),
    '2' => array('answers' => array('Bob', 'Charlie', 'Berger', 'Gurad'), 'result' => 'Berger'),
    '3' => array('answers' => array('yes', 'no'), 'result' => 'no'),
    '4' => array('answers' => array(1, 3, 6, 2), 'result' => 2),
    '5' => array('answers' => array(0.5, 2, 5, 0), 'result' => 0.5),
    '6' => array('answers' => array(2, 3, 4, 5), 'result' => 2),
    '7' => array('answers' => array(2, 3, 4, 5), 'result' => 4),
    '8' => array('answers' => array(2, 3, 6, 5), 'result' => 6),
    '9' => array('answers' => array(2, 0, 15, 50), 'result' => 50),
    // '10' => array('answers' => array(1, 3, 6, 7), 'result' => 5),
));